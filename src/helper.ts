import { NegativeOne, Two, Zero } from './constants';
import { Numeric, NumericValue, toNumeric } from './numeric';

export interface RangeOptions {
  includeMinimum?: boolean;
  includeMaximum?: boolean;
}

export const isRange = (value: NumericValue, minimum: NumericValue, maximum: NumericValue, options?: RangeOptions) => {
  try {
    const data = toNumeric(value);
    const defaultOptions: Required<RangeOptions> = { includeMaximum: true, includeMinimum: true };
    const { includeMaximum, includeMinimum } = options ? Object.assign({}, defaultOptions, options) : defaultOptions;
    const isOverLower = includeMinimum ? data.lessThan(minimum) : data.lessThanOrEqualTo(minimum);
    const isOverUpper = includeMaximum ? data.greaterThan(maximum) : data.greaterThanOrEqualTo(maximum);
    return !(isOverLower || isOverUpper);
  } catch (error) {
    return false;
  }
};

export const tryParse = (value: NumericValue) => {
  try {
    return toNumeric(value);
  } catch (error) {
    return false;
  }
};

export const isInteger = (value: NumericValue) => {
  const data = tryParse(value);
  return false !== data && data.isInteger();
};

export const isUnsignedIntegerBits = (bits: bigint, value: NumericValue) => {
  if (0n > bits || false === isInteger(value)) {
    return false;
  }

  const maximum = Two.toPower(bits);
  const minimum = Zero;
  const options: RangeOptions = { includeMinimum: true, includeMaximum: false };
  return isRange(value, minimum, maximum, options);
};

export const isSignedIntegerBits = (bits: bigint, value: NumericValue) => {
  if (0n > bits || false === isInteger(value)) {
    return false;
  }

  const maximum = Two.toPower(bits - 1n);
  const minimum = NegativeOne.times(maximum);
  const options: RangeOptions = { includeMinimum: true, includeMaximum: false };
  return isRange(value, minimum, maximum, options);
};

export const createArrayCompareFunction = (ascending: boolean = true) =>
  ascending
    ? (left: NumericValue, right: NumericValue) => Numeric.sub(left, right).toNumber()
    : (left: NumericValue, right: NumericValue) => Numeric.sub(right, left).toNumber();
