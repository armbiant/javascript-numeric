import { Decimal } from 'decimal.js';
import { Numeric } from './numeric';

export const getNumericConfig = () => {
  const { precision, rounding, minE, maxE, toExpNeg, toExpPos, crypto, modulo } = Numeric;
  const data: Required<Decimal.Config> = {
    precision,
    rounding,
    minE,
    maxE,
    toExpNeg,
    toExpPos,
    crypto,
    modulo,
    defaults: false,
  };

  return data;
};

export const setupNumericConfig = () => {
  const config: Decimal.Config = {
    precision: 256,
    //rounding: Numeric.ROUND_FLOOR,
    toExpNeg: Numeric.minE,
    toExpPos: Numeric.maxE,
    crypto: false,
  };

  Numeric.set({ defaults: true });
  Numeric.set(config);
};
