import * as constants from '@ethersproject/constants';
import { Numeric } from './numeric';

export const NegativeOne = new Numeric(-1);
export const Zero = new Numeric(0);
export const One = new Numeric(1);
export const Two = new Numeric(2);
export const WeiPerEther = new Numeric(constants.WeiPerEther);
export const MaxUint256 = new Numeric(constants.MaxUint256);
export const MaxInt256 = new Numeric(constants.MaxInt256);
export const MinInt256 = new Numeric(constants.MinInt256);
