# Numeric

A wrapper of Decimal.js

## Usage

See the source code for more details.

All functions are same as Decimal.js and can convert from:

- String
- Number
- BigInt
- Array of Number
- [bn.js](https://www.npmjs.com/package/bn.js)
- [BigInteger.js](https://www.npmjs.com/package/big-integer)
- [Big Numbers](https://www.npmjs.com/package/@ethersproject/bignumber)
- [Decimal.js](https://www.npmjs.com/package/decimal.js)

Examples:

```ts
import { Numeric } from '@agrozyme/numeric';
const test1 = new Numeric('1');
const test2 = new Numeric(2);
const test3 = new Numeric(3n);
const test4 = new Numeric([4]);
const test5 = new Numeric(new BN(5));
const test6 = new Numeric(bigInt('6'));
const test7 = new Numeric(BigNumber.from(7));
const test8 = new Numeric(new Decimal(8));
```

## Constants

Same as [Etehreum Constants](https://www.npmjs.com/package/@ethersproject/constants) bignumber constants but export to Numeric type

- NegativeOne
- Zero
- One
- Two
- WeiPerEther
- MaxUint256
- MaxInt256
- MinInt256

## Helper Functions

- tryParse: try parse value to Numeric

```ts
const test = tryParse('test');
//  test is false
```

- isRange: test value range

```ts
const test = isRange(0, -10n, '10');
```

- isInteger: test value is a integer

```ts
const test = isInteger('0.1');
```

- isUnsignedIntegerBits: test value is a unsigned integer that has significant bits
- isSignedIntegerBits: test value is a signed integer that has significant bits

```ts
const test1 = isUnsignedIntegerBits(8n, '256');
const test2 = isSignedIntegerBits(8n, '256');
```

- createArrayCompareFunction: create a simple array compare function

```ts
const ascending = createArrayCompareFunction(true);
const descending = createArrayCompareFunction(false);
```

## Format Functions

- formatValue: use [format-number](https://www.npmjs.com/package/format-number) to format a Numeric-Like value

```ts
const text = formatValue(1000, { prefix: '$' });
```

## Convert Functions

All convert functions are accept one Numeric-Like argument, except `toNumerics`

- toNumerics: convert a Numeric-Like array to a Numeric array

```ts
import { toNumerics } from '@agrozyme/numeric';
const test = toNumerics(['1', 2, 3n]);
```

- toNumeric: convert a Numeric-Like value to a Numeric value
- toIntegerString: convert a Numeric-Like value to a interger string
- toBigInt: convert a Numeric-Like value to a `native` bigint value
- toBN: convert a Numeric-Like value to a BN value
- toBigInteger: convert a Numeric-Like value to a BigInteger value
- toBigNumber: convert a Numeric-Like value to a BigNumber value

## Config Functions

- getNumericConfig: get NumericConfig that same as [Decimal.Config](https://mikemcl.github.io/decimal.js/#constructor-properties)
- setupNumericConfig: setup NumericConfig
